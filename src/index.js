/*
const express = require('express');
const app = express();
const React = require('react');
const renderToString = require('react-dom/server').renderToString;
const Home = require('./Components/Home').default;
*/
import express from 'express';
const app = express();
import renderer from './helpers/renderer'; // If you export using ES6 way import it using ES6 way.

app.use(express.static('public'));
app.get('/', (request, response) => {
    response.send(renderer(request));
});

app.listen(3000, () => {
    console.log('Running on port 3000');
});
