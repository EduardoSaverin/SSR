import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Routes from '../Routes/routes';

ReactDOM.hydrate(<BrowserRouter><Routes /></BrowserRouter>, document.getElementById('root'));