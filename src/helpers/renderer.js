import React from 'react';
import { renderToString } from 'react-dom/server';
import Routes from '../Routes/routes';
import { StaticRouter } from 'react-router-dom';

export default (req) => {
    const content = renderToString(
        <StaticRouter context={{}} location={req.path}>
            <Routes />
        </StaticRouter>
    );
    var html = `
        <html>
            <head></head>
            <body>
                <div id="root">${content}</div>
            </body>
            <script src='bundle.js'></script>
        </html>
    `
    return html;
}