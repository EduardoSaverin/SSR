import React from 'react';
import { Route } from 'react-router-dom';
import Home from '../Components/Home';

export default () => {
    return (
        <div>
            <Route exact component={Home} path="/" />
        </div>
    )
}